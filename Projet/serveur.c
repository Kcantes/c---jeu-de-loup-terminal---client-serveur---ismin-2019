#include "pse.h"
#include <time.h>
#include "utils.h"

#define CMD "serveur"
#define NB_WORKERS 10
#define TAILLE_TERRAIN 20
// init --------------------------------------------------------
void initTerrain();
void *WaitForWin();
void creerCohorteWorkers(void);
// run ---------------------------------------------------------
int chercherWorkerLibre(void);
void *threadWorker(void *arg);
void sessionClient(int canal, int id);
void *connectToPlayer(void *i);
// utility -----------------------------------------------------
int ecrireJournal(char *buffer);
void remiseAZeroJournal(void);
void lockMutexJournal(void);
void unlockMutexJournal(void);
void lockMutexJeu(void);
void unlockMutexJeu(void);
void lockMutexCanal(int numWorker);
void unlockMutexCanal(int numWorker);
void lockMutexJeu();
void unlockMutexJeu();

int fdJournal;
DataSpec dataSpec[NB_WORKERS];
sem_t semWorkersLibres;

// la remise a zero du journal modifie le descripteur du fichier, il faut donc
// proteger par un mutex l'ecriture dans le journal et la remise a zero
pthread_mutex_t mutexJournal;

// pour l'acces au canal d'un worker peuvant etre en concurrence la recherche
// d'un worker libre et la mise à -1 du canal par le worker
pthread_mutex_t mutexCanal[NB_WORKERS];
//le terrain de jeu
char jeu[TAILLE_TERRAIN * TAILLE_TERRAIN] = {'s'};
//mutex pour modifier le tableau de jeu
pthread_mutex_t mutex_TableauDeJeu = PTHREAD_MUTEX_INITIALIZER;

//l'état des joueurs
char playerStatus[NB_WORKERS];
//leurs positions
int playerPos[NB_WORKERS];
//ips des joueurs
char *ips[NB_WORKERS];
//ports des joueurs
char *ports[NB_WORKERS];
//envoyer les modifications à tous les joueurs ?
int push[NB_WORKERS];
//threads d'envoi du serveur aux joueurs
pthread_t connectthread[NB_WORKERS];
//thread pour le cas ou il ne reste qu'un joueur
pthread_t WinThread;
//le-dit joeur a-t-il gagné ?
int endWin;

int main(int argc, char *argv[])
{
  initTerrain();
  Affiche_tab(jeu, TAILLE_TERRAIN);
  pthread_create(&WinThread, NULL, WaitForWin, NULL);
  short port = 3400;
  int ecoute, canal, ret;
  struct sockaddr_in adrEcoute, adrClient;
  unsigned int lgAdrClient;
  int numWorkerLibre;

  fdJournal = open("journal.log", O_WRONLY | O_CREAT | O_APPEND, 0644);
  if (fdJournal == -1)
    erreur_IO("ouverture journal");

  creerCohorteWorkers();

  ret = sem_init(&semWorkersLibres, 0, NB_WORKERS);
  if (ret == -1)
    erreur_IO("init sem workers libres");

  printf("%s: creating a socket\n", CMD);
  ecoute = socket(AF_INET, SOCK_STREAM, 0);
  if (ecoute < 0)
    erreur_IO("socket");

  adrEcoute.sin_family = AF_INET;
  adrEcoute.sin_addr.s_addr = INADDR_ANY;
  adrEcoute.sin_port = htons(port);
  printf("%s: binding to INADDR_ANY address on port %d\n", CMD, port);
  ret = bind(ecoute, (struct sockaddr *)&adrEcoute, sizeof(adrEcoute));
  if (ret < 0)
    erreur_IO("bind");

  printf("%s: listening to socket\n", CMD);
  ret = listen(ecoute, 5);
  if (ret < 0)
    erreur_IO("listen");

  while (VRAI)
  {
    printf("%s: accepting a connection\n", CMD);
    canal = accept(ecoute, (struct sockaddr *)&adrClient, &lgAdrClient);
    if (canal < 0)
      erreur_IO("accept");

    printf("%s: adr %s, port %hu\n", CMD,
           stringIP(ntohl(adrClient.sin_addr.s_addr)), ntohs(adrClient.sin_port));
    ret = sem_wait(&semWorkersLibres); // attente d'un worker libre
    if (ret == -1)
      erreur_IO("wait sem workers libres");
    numWorkerLibre = chercherWorkerLibre();
    ips[numWorkerLibre] = stringIP(ntohl(adrClient.sin_addr.s_addr));

    dataSpec[numWorkerLibre].canal = canal;
    sem_post(&dataSpec[numWorkerLibre].sem); // reveil du worker
    if (ret == -1)
      erreur_IO("post sem worker");
  }

  if (close(ecoute) == -1)
    erreur_IO("fermeture ecoute");

  if (close(fdJournal) == -1)
    erreur_IO("fermeture journal");

  exit(EXIT_SUCCESS);
}
// cree le terrain avec des obstacles
void initTerrain()
{
  remiseAZeroJournal();
  srand(time(NULL));
  for (int i = 0; i < TAILLE_TERRAIN * TAILLE_TERRAIN; i++)
  {
    jeu[i] = 's';
    int k = rand() % 100;
    if (k < 10)
    {
      jeu[i] = 'o';
    }
  }
}
//envoyer les donnees aux joueurs
void SetPush()
{
  for (int i = 0; i < NB_WORKERS; i++)
  {
    push[i] = 1;
  }
}
//permet au dernier joueur humain de gagner au bout de 5 secondes
void *WaitForWin()
{
  while (1)
  {
    int *stats = Joueurs(playerStatus, NB_WORKERS);
    if (stats[0] > 2)
    {
      if (stats[1] == 1)
      {
        printf("%s: One player remaining\n", CMD);
        usleep(5000000);
        int *stats = Joueurs(playerStatus, NB_WORKERS);
        if (stats[1] == 1)
        {
          endWin = 1;
          SetPush();
        }
      }
    }
    usleep(1000);
  }
}
//cree les workers de connexion
void creerCohorteWorkers(void)
{
  int i, ret;

  for (i = 0; i < NB_WORKERS; i++)
  {
    dataSpec[i].canal = -1;
    dataSpec[i].tid = i;
    ret = sem_init(&dataSpec[i].sem, 0, 0);
    if (ret == -1)
      erreur_IO("init sem worker");

    ret = pthread_create(&dataSpec[i].id, NULL, threadWorker, &dataSpec[i]);
    if (ret != 0)
      erreur_IO("creation thread");
  }
}

// retourne le no. du worker libre trouve ou -1 si pas de worker libre
int chercherWorkerLibre(void)
{
  int numWorkerLibre = -1, i = 0, canal;

  while (numWorkerLibre < 0 && i < NB_WORKERS)
  {
    lockMutexCanal(i);
    canal = dataSpec[i].canal;
    unlockMutexCanal(i);

    if (canal == -1)
      numWorkerLibre = i;
    else
      i++;
  }

  return numWorkerLibre;
}
//assigne les threads des session
void *threadWorker(void *arg)
{
  DataSpec *dataTh = (DataSpec *)arg;
  int ret;

  while (VRAI)
  {
    ret = sem_wait(&dataTh->sem); // attente reveil par le thread principal
    if (ret == -1)
      erreur_IO("wait sem worker");

    printf("%s: worker %d reveil\n", CMD, dataTh->tid);

    sessionClient(dataTh->canal, dataTh->tid);

    lockMutexCanal(dataTh->tid);
    dataTh->canal = -1;
    unlockMutexCanal(dataTh->tid);

    ret = sem_post(&semWorkersLibres); // incrementer le nb de workers libres
    if (ret == -1)
      erreur_IO("post sem workers libres");

    printf("%s: worker %d sommeil\n", CMD, dataTh->tid);
  }

  pthread_exit(NULL);
}
//reçoit toutes les commandes des joueurs et met à jour le tableau
void sessionClient(int canal, int id)
{
  endWin = 0;
  int fin = FAUX;
  char ligne[LIGNE_MAX];
  int lgLue;
  //ip & port
  lireLigne(canal, ligne);
  int len = strlen(ligne);
  ligne[len] = 0;
  strcpy(ips[id], ligne);

  lireLigne(canal, ligne);
  len = strlen(ligne);
  ligne[len] = 0;
  ports[id] = (char *)malloc(len * sizeof(char));
  strcpy(ports[id], ligne);
  //player num
  char playerNum[12];
  sprintf(playerNum, "%d", id);
  char message[LIGNE_MAX] = "player ";
  strcat(message, playerNum);

  ecrireJournal(message);
  strcpy(ligne, "connect");
  ecrireJournal(ligne);

  //faire spawn le joueur
  int spwn = rand() % (TAILLE_TERRAIN * TAILLE_TERRAIN);
  while (jeu[spwn] != 's')
    spwn = rand() % (TAILLE_TERRAIN * TAILLE_TERRAIN);

  pthread_create(&connectthread[id], NULL, connectToPlayer, &dataSpec[id].tid);
  SetPush();
  usleep(6000000);
  lockMutexJeu();
  if (id == 0)
    jeu[spwn] = 'L';
  else
    jeu[spwn] = 'J';
  unlockMutexJeu();
  SetPush();

  while (!fin)
  {
    playerPos[id] = spwn;
    lgLue = lireLigne(canal, ligne);
    ecrireJournal(message);
    if (lgLue < 0)
      erreur_IO("lireLigne");
    else if (lgLue == 0)
      erreur("RAGEQUIT : interruption client\n");

    printf("%s: reception %d octets : \"%s\"\n", CMD, lgLue, ligne);

    if (strcmp(ligne, "fin") == 0)
    {
      printf("%s: fin client\n", CMD);
      ecrireJournal(ligne);
      fin = VRAI;
      continue;
    }
    else if (ecrireJournal(ligne) != -1)
    {

      printf("%s: ligne de %d octets ecrite dans journal\n", CMD, lgLue);
    }
    else
      erreur_IO("ecriture journal");

    int i = -1;
    if (strcmp(ligne, "haut\n") == 0)
      i = 0;
    if (strcmp(ligne, "bas\n") == 0)
      i = 1;
    if (strcmp(ligne, "droite\n") == 0)
      i = 2;
    if (strcmp(ligne, "gauche\n") == 0)
      i = 3;
    switch (i)
    {
    case 0:
      if (spwn - TAILLE_TERRAIN < 0)
      {
        break;
      }

      if (jeu[spwn - TAILLE_TERRAIN] == 's')
      {
        lockMutexJeu();
        jeu[spwn - TAILLE_TERRAIN] = jeu[spwn];
        jeu[spwn] = 's';
        unlockMutexJeu();
        spwn = spwn - TAILLE_TERRAIN;
        break;
      }

      if (jeu[spwn] == 'L')
      {
        if (jeu[spwn - TAILLE_TERRAIN] == 'J')
        {
          jeu[spwn - TAILLE_TERRAIN] = 'L';
        }
      }
      break;
    case 1:
      if (spwn + TAILLE_TERRAIN >= TAILLE_TERRAIN * TAILLE_TERRAIN)
      {
        break;
      }

      if (jeu[spwn + TAILLE_TERRAIN] == 's')
      {
        lockMutexJeu();
        jeu[spwn + TAILLE_TERRAIN] = jeu[spwn];
        jeu[spwn] = 's';
        unlockMutexJeu();
        spwn = spwn + TAILLE_TERRAIN;
        break;
      }

      if (jeu[spwn] == 'L')
      {
        if (jeu[spwn + TAILLE_TERRAIN] == 'J')
        {
          jeu[spwn + TAILLE_TERRAIN] = 'L';
        }
      }
      break;
    case 2:
      if ((spwn) % TAILLE_TERRAIN == TAILLE_TERRAIN - 1)
      {
        break;
      }

      if (jeu[spwn + 1] == 's')
      {
        lockMutexJeu();
        jeu[spwn + 1] = jeu[spwn];
        jeu[spwn] = 's';
        unlockMutexJeu();
        spwn = spwn + 1;
        break;
      }

      if (jeu[spwn] == 'L')
      {
        if (jeu[spwn + 1] == 'J')
        {
          jeu[spwn + 1] = 'L';
        }
      }
      break;
    case 3:
      if ((spwn) % TAILLE_TERRAIN == 0)
      {
        break;
      }

      if (jeu[spwn - 1] == 's')
      {
        lockMutexJeu();
        jeu[spwn - 1] = jeu[spwn];
        jeu[spwn] = 's';
        unlockMutexJeu();
        spwn = spwn - 1;
        break;
      }

      if (jeu[spwn] == 'L')
      {
        if (jeu[spwn - 1] == 'J')
        {
          jeu[spwn - 1] = 'L';
        }
      }
      break;
    default:
      break;
    }
    SetPush();
  } // fin while
  playerStatus[id] = 's';
  if (close(canal) == -1)
    erreur_IO("fermeture canal");
  lockMutexJeu();
  jeu[spwn] = 's';
  unlockMutexJeu();
  SetPush();
}
//permet l'envoi des données aux clients
void *connectToPlayer(void *i)
{
  char *ip = ips[*(int *)i];
  char *port = ports[*(int *)i];
  int sock, ret;
  struct sockaddr_in *adrCli;
  char playerNum[12];
  sprintf(playerNum, "%d", *(int *)i);
  char message[LIGNE_MAX] = "player ";
  strcat(message, playerNum);
  printf("%s: creating a socket\n", CMD);
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0)
    erreur_IO("socket");

  printf("%s: DNS resolving for %s, port %s\n", CMD, ip, port);
  //ip="localhost";
  adrCli = resolv(ip, port);
  if (adrCli == NULL)
    erreur("adresse %s port %s inconnus\n", ip, port);

  printf("%s: adr %s, port %hu\n", CMD,
         ip,
         ntohs(adrCli->sin_port));

  printf("%s: connecting the socket\n", CMD);
  ret = connect(sock, (struct sockaddr *)adrCli, sizeof(struct sockaddr_in));
  if (ret < 0)
    erreur_IO("connect");
  SetPush();
  char ligne[LIGNE_MAX];

  while (dataSpec[*(int *)i].canal > 0)
  {
    playerStatus[*(int *)i] = jeu[playerPos[*(int *)i]];
    if (push[*(int *)i] == 1)
    {
      ecrireLigne(sock, jeu);
      int *stats = Joueurs(playerStatus, NB_WORKERS);
      if (stats[0] > 2)
      {
        if (endWin == 1)
        {
          if (playerStatus[*(int *)i] == 'J')//gagne
          {
            strcpy(ligne, "win");
            printf("%s: player %d wins !\n", CMD, *(int *)i);
          }
          else//perd
          {
            strcpy(ligne, "lose");
            printf("%s: player %d lost\n", CMD, *(int *)i);
          }
          ecrireLigne(sock, ligne);
          strcat(message, " ");
          strcat(message, ligne);
          strcat(message, " : fin de partie");
          ecrireJournal(message);
          usleep(200);
          playerStatus[*(int *)i] = 's';
        }
        else if (stats[0] == stats[2])//les joueurs sont-ils tous des loups
        {
          strcpy(ligne, "winL");
          printf("%s: les loups gagnent\n", CMD);
          ecrireLigne(sock, ligne);
          strcat(message, " victoire des loups");
          strcat(message, " : fin de partie");
          ecrireJournal(message);
          usleep(200);
          playerStatus[*(int *)i] = 's';
        }
      }
      push[*(int *)i] = 0;
    }
    usleep(50);//pour l'optimisation
  }
  pthread_exit(NULL);
}
//l'historique de partie
int ecrireJournal(char *buffer)
{
  int lgLue;

  lockMutexJournal();
  lgLue = ecrireLigne(fdJournal, buffer);
  unlockMutexJournal();
  return lgLue;
}
// le fichier est ferme et rouvert vide
void remiseAZeroJournal(void)
{
  lockMutexJournal();

  if (close(fdJournal) == -1)
    erreur_IO("raz journal - fermeture");

  fdJournal = open("journal.log", O_WRONLY | O_TRUNC | O_APPEND, 0644);
  if (fdJournal == -1)
    erreur_IO("raz journal - ouverture");

  unlockMutexJournal();
}

void lockMutexJournal(void)
{
  int ret;
  ret = pthread_mutex_lock(&mutexJournal);
  if (ret != 0)
    erreur_IO("lock mutex journal");
}

void unlockMutexJournal(void)
{
  int ret;
  ret = pthread_mutex_unlock(&mutexJournal);
  if (ret != 0)
    erreur_IO("lock mutex journal");
}

void lockMutexJeu(void)
{
  int ret;
  ret = pthread_mutex_lock(&mutex_TableauDeJeu);
  if (ret != 0)
    erreur_IO("lock mutex Jeu");
}

void unlockMutexJeu(void)
{
  int ret;
  ret = pthread_mutex_unlock(&mutex_TableauDeJeu);
  if (ret != 0)
    erreur_IO("lock mutex Jeu");
}

void lockMutexCanal(int numWorker)
{
  int ret;
  ret = pthread_mutex_lock(&mutexCanal[numWorker]);
  if (ret != 0)
    erreur_IO("lock mutex dataspec");
}

void unlockMutexCanal(int numWorker)
{
  int ret;
  ret = pthread_mutex_unlock(&mutexCanal[numWorker]);
  if (ret != 0)
    erreur_IO("lock mutex dataspec");
}
