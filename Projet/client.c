#include "pse.h"
#include "utils.h"
#include <stdio.h>
#define CMD "client"
#define TAILLE_TERRAIN 20

//reçoit les infos du serveur
void *receive(void *arg);

char jeu[TAILLE_TERRAIN * TAILLE_TERRAIN] = {'s'};

int fin = FAUX;

int state = 0;//le joueur a-t-il spawné

pthread_t idthread;//thread de receive

char port_client[10];
//sock d'envoi au serveur
int sock;

int main(int argc, char *argv[])
{
  int ret;
  struct sockaddr_in *adrServ;
  char ligne[LIGNE_MAX];
  int lgEcr;
  if (argc != 4)
    erreur("usage: %s ip_serveur ip_machine port_client\n", argv[0]);

  strcpy(port_client, argv[3]);
  printf("%s: creating a socket\n", CMD);
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0)
    erreur_IO("socket");

  printf("%s: DNS resolving for %s, port %s\n", CMD, argv[1], "3400");
  adrServ = resolv(argv[1], "3400");
  if (adrServ == NULL)
    erreur("adresse %s port %s inconnus\n", argv[1], "3400");

  printf("%s: adr %s, port %hu\n", CMD,
         stringIP(ntohl(adrServ->sin_addr.s_addr)),
         ntohs(adrServ->sin_port));

  printf("%s: connecting the socket\n", CMD);
  ret = connect(sock, (struct sockaddr *)adrServ, sizeof(struct sockaddr_in));
  if (ret < 0)
    erreur_IO("connect");
  ecrireLigne(sock, argv[2]);
  ecrireLigne(sock, port_client);
  pthread_create(&idthread, NULL, receive, NULL);

  while (!fin)
  {
    while (state == 0)
	{
		usleep(100);
	}
    char touche;
    scanf("%c", &touche);
    if (fin == VRAI)
      break;
    switch (touche)
    { 
    case 'z':
      strcpy(ligne, "haut");
      lgEcr = ecrireLigne(sock, ligne);
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");
      break;
    case 's':
      strcpy(ligne, "bas");
      lgEcr = ecrireLigne(sock, ligne);
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");
      // code for arrow down
      break;
    case 'd':
      strcpy(ligne, "droite");
      lgEcr = ecrireLigne(sock, ligne);
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");
      // code for arrow right
      break;
    case 'q':
      strcpy(ligne, "gauche");
      lgEcr = ecrireLigne(sock, ligne);
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");
      // code for arrow left
      break;
    case 'p':
      strcpy(ligne, "fin");
      lgEcr = ecrireLigne(sock, ligne);
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");
      fin = VRAI;
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");
      // code for arrow left
	  if (close(sock) == -1)
		erreur_IO("close socket");
	  printf("\nFIN CLIENT\n");
	  exit(EXIT_SUCCESS);
      break;
    }
  }
  pthread_join(idthread, NULL);

  if (close(sock) == -1)
    erreur_IO("close socket");

  printf("\nFIN CLIENT\n");
  exit(EXIT_SUCCESS);

  return 0;
}
//reçoit les donnees et les affiche
void *receive(void *arg)
{
  short port = (short)atoi(port_client);
  int ecoute, canal, ret;
  struct sockaddr_in adrEcoute, adrClient;
  unsigned int lgAdrClient;
  char ligne[LIGNE_MAX];
  ecoute = socket(AF_INET, SOCK_STREAM, 0);
  if (ecoute < 0)
    erreur_IO("socket");
  adrEcoute.sin_family = AF_INET;
  adrEcoute.sin_addr.s_addr = INADDR_ANY;
  adrEcoute.sin_port = htons(port);
  ret = bind(ecoute, (struct sockaddr *)&adrEcoute, sizeof(adrEcoute));
  if (ret < 0)
    erreur_IO("bind");
  ret = listen(ecoute, 5);
  if (ret < 0)
    erreur_IO("listen");
  canal = accept(ecoute, (struct sockaddr *)&adrClient, &lgAdrClient);
  if (canal < 0)
    erreur_IO("accept");

  CleanConsole();
  Affiche_dessin();
  usleep(5000000);
  state = 1;
  while (!fin)
  {
    lireLigne(canal, ligne);
    if (strcmp(ligne, "winL") == 0)//les loups gagnent
    {
      fin = VRAI;
      usleep(1000000);
      CleanConsole();
      WinL();
      strcpy(ligne, "fin");
      int lgEcr = ecrireLigne(sock, ligne);
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");
      usleep(5000000);
      break;
    }
    else if (strcmp(ligne, "win") == 0)//le joueur gagne
    {
      fin = VRAI;
      usleep(1000000);
      CleanConsole();
      Win();
      strcpy(ligne, "fin");
      int lgEcr = ecrireLigne(sock, ligne);
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");
      usleep(5000000);
      break;
    }
    else if (strcmp(ligne, "lose") == 0)//le joueur perd
    {
      fin = VRAI;
      usleep(1000000);
      CleanConsole();
      Lose();
      strcpy(ligne, "fin");
      int lgEcr = ecrireLigne(sock, ligne);
      if (lgEcr == -1)
        erreur_IO("ecrire ligne");

      usleep(3000000);
      break;
    }

    strcpy(jeu, ligne);
    CleanConsole();
    Affiche_tab(jeu, TAILLE_TERRAIN);
  }
  if (close(ecoute) == -1)
    erreur_IO("fermeture ecoute");

  exit(EXIT_SUCCESS);
}