#include <stdio.h>
#include <stdlib.h>

void CleanConsole();
//affiche le tableau de jeu
void Affiche_tab(char *jeu, int taille);
//affiche le dessin d'introduction a la connection d'un client
void Affiche_dessin();
//affiche les loups gagnent
void WinL();
//affiche vous avez perdu, cheh!
void Lose();
//affiche vous avez gagné
void Win();
//compte le nombre de joueurs etc
int *Joueurs(char *players, int taille);
